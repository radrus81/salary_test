import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import './sass/app.scss'
import CardForm from './components/CardForm/CardForm'

const App: React.FC = () => {
  return (
    <Container className="mt-3">
      <Row>
        <Col>
          <CardForm />
        </Col>
      </Row>
    </Container>
  )
}

export default App
