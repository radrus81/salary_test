import { reduxForm, formValueSelector } from 'redux-form'
import { connect } from 'react-redux'
import SalaryForm from '../components/SalaryForm/SalaryForm'

let SalaryFormWithRedux: any = reduxForm({
  form: 'SalaryForm',
})(SalaryForm)

const calcSalary = (sumMoney: number, isSwitchNdfl: boolean) => {
  let sumNdfl = sumMoney / 0.87 - sumMoney
  let moneyToHard = sumMoney

  if (!isSwitchNdfl) {
    sumNdfl = (sumMoney * 13) / 100
    moneyToHard = sumMoney - sumNdfl
  }
  return { sumNdfl, moneyToHard }
}

const selector = formValueSelector('SalaryForm')

SalaryFormWithRedux = connect((state) => {
  const data = {
    radios: 'salaryInMonth',
    sum: 0,
    switchNdfl: true,
  }

  const typeRadio = selector(state, 'radios')
  const userSum = selector(state, 'sum')
  const isSwitchNdfl = selector(state, 'switchNdfl')
  const { sumNdfl, moneyToHard } = calcSalary(userSum, isSwitchNdfl)
  return {
    typeRadio,
    sumNdfl,
    moneyToHard,
    isSwitchNdfl,
    initialValues: data,
  }
})(SalaryFormWithRedux)

export default SalaryFormWithRedux
