import React from 'react'
import { Card } from 'react-bootstrap'
import './CardForm.scss'
import SalaryFormReduxForm from '../../containers/SalaryForm'

const CardForm: React.FC = () => {
  return (
    <Card className="wrapper">
      <Card.Body>
        <Card.Subtitle className="mb-2 text-muted">Сумма</Card.Subtitle>
        <SalaryFormReduxForm />
      </Card.Body>
    </Card>
  )
}

export default CardForm
