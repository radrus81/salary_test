import React from 'react'
import './CalcInfo.scss'
import { currency } from '../../utils/currency'

interface PropsCalcInfo {
  sumNdfl: string
  moneyToHard: string
}

const CalcInfo: React.FC<PropsCalcInfo> = ({ sumNdfl, moneyToHard }) => {
  const total = +moneyToHard + +sumNdfl
  return (
    <>
      <div className="calc_info ml-3 mb-2">
        <label className="calc_info_sum">{currency(moneyToHard)}</label>
        <label className="calc_info_text">
          сотрудник будет получать на руки
        </label>
      </div>
      <div className="calc_info ml-3 mb-2">
        <label className="calc_info_sum">{currency(sumNdfl)}</label>
        <label className="calc_info_text">НДФЛ</label>
      </div>
      <div className="calc_info ml-3 mb-2">
        <label className="calc_info_sum">{currency(total)}</label>
        <label className="calc_info_text">за сотрудника в месяц</label>
      </div>
    </>
  )
}

export default CalcInfo
