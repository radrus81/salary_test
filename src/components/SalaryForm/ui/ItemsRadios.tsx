import React from 'react'
import { Form, FormCheck } from 'react-bootstrap'
import { Field } from 'redux-form'
import TooltipMROT from './TooltipMROT'

interface PropsTitleSalary {
  name: string
  label: string
  isIconInfo: boolean
  value: string
}

const titleSalary: PropsTitleSalary[] = [
  {
    name: 'salary',
    label: 'Оплата за месяц',
    isIconInfo: false,
    value: 'salaryInMonth',
  },
  {
    name: 'salary',
    label: 'МРОТ',
    isIconInfo: true,
    value: 'salaryMin',
  },
  {
    name: 'salary',
    label: 'Оплата за день',
    isIconInfo: false,
    value: 'salaryInDay',
  },
  {
    name: 'salary',
    label: 'Оплата за час',
    isIconInfo: false,
    value: 'salaryInHour',
  },
]

const ItemsRadios: React.FC<PropsTitleSalary | any> = ({
  name,
  label,
  isIconInfo,
  isChecked,
  value,
  ...props
}): React.ReactElement<any> => {
  return (
    <>
      {titleSalary.map((item) => {
        return (
          <div key={item.value}>
            <FormCheck.Input
              className="salary_form_radio"
              type="radio"
              name={item.name}
              defaultChecked={item.value === props.input.value ? true : false}
              onChange={props.input.onChange}
              value={item.value}
            />
            <Form.Check.Label className="salary_form_label">
              {item.label}
            </Form.Check.Label>
            {item.isIconInfo ? <TooltipMROT /> : null}
          </div>
        )
      })}
    </>
  )
}

const ItemsRadioRedux: React.FC = () => {
  return (
    <FormCheck>
      <Field component={ItemsRadios} name="radios" />
    </FormCheck>
  )
}

export default ItemsRadioRedux
