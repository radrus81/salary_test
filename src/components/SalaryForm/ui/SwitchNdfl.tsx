import React from 'react'
import { Form, FormCheck } from 'react-bootstrap'
import { Field } from 'redux-form'

interface PropsFieldSwitch {
  input: any
}

type InProps = {
  isSwitchNdfl: boolean
}

const FieldSwitch: React.FC<PropsFieldSwitch> = ({
  input,
}): React.ReactElement => {
  return (
    <Form.Check
      type="switch"
      id="field-switch"
      onChange={input.onChange}
      defaultChecked={input.value}
    />
  )
}

const SwitchNdfl: React.FC<InProps> = ({ isSwitchNdfl }) => {
  return (
    <FormCheck className="salary_form_block_ndfl">
      <FormCheck.Label
        className={
          !isSwitchNdfl
            ? 'salary_form_label_ndfl active'
            : 'salary_form_label_ndfl'
        }
      >
        Указать с НДФЛ
      </FormCheck.Label>
      <Field component={FieldSwitch} name="switchNdfl" />
      <FormCheck.Label
        className={
          isSwitchNdfl
            ? 'salary_form_label_ndfl active'
            : 'salary_form_label_ndfl'
        }
      >
        Без НДФЛ
      </FormCheck.Label>
    </FormCheck>
  )
}

export default SwitchNdfl
