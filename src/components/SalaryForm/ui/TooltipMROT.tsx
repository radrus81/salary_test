import React from 'react'
import { OverlayTrigger, Tooltip } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'

const TooltipMROT: React.FC = () => {
  return (
    <OverlayTrigger
      placement={'right'}
      overlay={
        <Tooltip id={`tooltip-rigth`}>
          Минимальный размер оплаты труда. Разный для разных регионов.
        </Tooltip>
      }
    >
      <FontAwesomeIcon icon={faInfoCircle} size="1x" className="ml-2 mt-1" />
    </OverlayTrigger>
  )
}

export default TooltipMROT
