import React from 'react'
import { Form, FormCheck } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRubleSign } from '@fortawesome/free-solid-svg-icons'
import { Field } from 'redux-form'

interface PropsField {
  input: any
}

const FieldInputSum: React.FC<PropsField> = ({ input }) => {
  return (
    <FormCheck className="salary_form_block input_sum">
      <Form.Control
        type="number"
        step="0.01"
        min="0"
        placeholder="Введите сумму"
        onChange={input.onChange}
        className="salary_form_for_summ mt-2"
        value={input.value ? input.value : ''}
      />
      <FontAwesomeIcon icon={faRubleSign} size="1x" className="ml-2 mt-1" />
    </FormCheck>
  )
}

const InputSum: React.FC = () => {
  return <Field component={FieldInputSum} name="sum" />
}

export default InputSum
