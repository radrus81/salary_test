import React from 'react'
import { Form } from 'react-bootstrap'
import './SalaryForm.scss'
import ItemsRadios from './ui/ItemsRadios'
import SwitchNdfl from './ui/SwitchNdfl'
import InputSum from './ui/InputSum'
import CalcInfo from '../CalcInfo/CalcInfo'

const SalaryForm: React.FC = (props: any) => {
  const { typeRadio, sumNdfl, moneyToHard, isSwitchNdfl } = props

  return (
    <Form className="salary_form mt-3">
      <Form.Group className="mb-3 ml-3">
        <ItemsRadios />
        {typeRadio !== 'salaryMin' ? (
          <>
            <SwitchNdfl isSwitchNdfl={isSwitchNdfl} />
            <InputSum />
          </>
        ) : (
          <div className="alert alert-warning infoMROT">
            Минимальная оплата труда в РФ на 2021 года равна 12 392 рубля
          </div>
        )}

        {typeRadio === 'salaryInMonth' && (
          <CalcInfo sumNdfl={sumNdfl} moneyToHard={moneyToHard} />
        )}
      </Form.Group>
    </Form>
  )
}

export default SalaryForm
